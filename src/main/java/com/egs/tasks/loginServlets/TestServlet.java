package com.egs.tasks.loginServlets;

import com.egs.tasks.commons.Severity;
import com.egs.tasks.commons.Status;
import com.egs.tasks.service.TaskService;
import com.egs.tasks.service.UserService;
import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.service.dtoes.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

@WebServlet("/index")
public class TestServlet extends HttpServlet {
    private TaskService taskService;
    private UserService userService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        userService = applicationContext.getBean(UserService.class);
        taskService = applicationContext.getBean(TaskService.class);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        UserDto creator = new UserDto();
        creator.setNameSurname("Creator user");
        creator.setPasswordHash("abcd");
        creator.setUsername("user");

        UserDto assignee = new UserDto();
        assignee.setNameSurname("Assignee user");
        assignee.setPasswordHash("1234");
        assignee.setUsername("admin");

        TaskDto task = new TaskDto();
        task.setComments(Arrays.asList("AAA", "BBB", "CCC"));
        task.setCreatedDate(new Date());
        task.setSeverity(Severity.EASY);
        task.setStatus(Status.NEW);
        task.setText("Dance");
//        task.setAssignee(assignee);
//        task.setReporter(creator);

//        creator.createTask(task);
//        assignee.addToAssignedTasks(task);

        if (request.getParameter("button1") != null) {
            Long creatorId = userService.createUser(creator);
            creator.setId(creatorId);

            Long assId = userService.createUser(assignee);
            assignee.setId(assId);

            Long id = taskService.createTask(task);
            task.setId(id);
            userService.assignTask(task, creator, assignee);
//            creator.addToCreatedTasks(task);
//            assignee.addToAssignedTasks(task);

        } else if (request.getParameter("button2") != null) {
            taskService.addComment(3L, "New New Comment");

        } else if (request.getParameter("button3") != null) {
//            userService.getAllUsernames();
//            userService.isExistingUserByGivenUsername("aaa");
            taskService.getAssignedTasks(2L);
        } else {
            // ???
        }

    }
}
