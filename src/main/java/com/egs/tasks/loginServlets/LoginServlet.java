package com.egs.tasks.loginServlets;

import com.egs.tasks.service.UserService;
import com.egs.tasks.service.dtoes.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        userService = applicationContext.getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String passwordHash = LoginHelper.getHashcode(password);
        Optional<UserDto> optionalUser = userService.checkCredentials(username, passwordHash);
        if (optionalUser.isPresent()) {
            req.getSession().setAttribute("nameSurname", username);
            req.getSession().setAttribute("currentUser", optionalUser.get());
            req.getSession().setAttribute("isSuccessful", true);
            resp.sendRedirect(req.getContextPath() + "/home");
        } else {
            req.getSession().setAttribute("isSuccessful", false);
            resp.sendRedirect("login.jsp");
        }
    }
}
