package com.egs.tasks.loginServlets;

import com.egs.tasks.service.UserService;
import com.egs.tasks.service.dtoes.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        userService = applicationContext.getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/signup.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String nameSurname = req.getParameter("nameSurname");
        String email = req.getParameter("email");
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        boolean isExistingUsername = userService.isExistingUserByGivenUsername(username);
        if(isExistingUsername){
            req.getSession().setAttribute("isExistingUsername", true );
            req.getServletContext().getRequestDispatcher("/signup.jsp").forward(req, resp);  //  /singup.jsp, not /signup  ???
            return;
        }
        req.getSession().setAttribute("isExistingUsername", false);
        String password_hash = LoginHelper.getHashcode(password);
        UserDto newUser = new UserDto(nameSurname, email, username, password_hash);
        userService.createUser(newUser);
        resp.sendRedirect( "login");

    }
}
