package com.egs.tasks.loginServlets;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginHelper {

     static String getHashcode(String password) {
        String hashCode = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] passBytes = password.getBytes();
            byte[] passHash = messageDigest.digest(passBytes);
            hashCode = DatatypeConverter.printHexBinary(passHash).toLowerCase();

        } catch (NoSuchAlgorithmException e) {
//            logger.error("Error during hashing the user's password.", e);
        }
        return hashCode;
    }
}
