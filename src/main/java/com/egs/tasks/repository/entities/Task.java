package com.egs.tasks.repository.entities;

import com.egs.tasks.commons.Severity;
import com.egs.tasks.commons.Status;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLInsert;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column
    private String title;
    @Column
    private String text;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Column
    @Enumerated(EnumType.STRING)
    private Status status;
    @Column
    @Enumerated(EnumType.STRING)
    private Severity severity;
    @ElementCollection(fetch = FetchType.LAZY)
    private List<String> comments = new ArrayList<>();

    @ManyToOne
    private User reporter;
    @ManyToOne
    private User assignee;

    public Task() {
    }

    public Task(Long id, String title, String text, Date createdDate, Date expirationDate, Status status, Severity severity, List<String> comments, User reporter, User assignee) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.createdDate = createdDate;
        this.expirationDate=expirationDate;
        this.status = status;
        this.severity = severity;
        this.comments = comments;
        this.reporter = reporter;
        this.assignee = assignee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public User getReporter() {
        return reporter;
    }

    public void setReporter(User reporter) {
        this.reporter = reporter;
    }

    public User getAssignee() {
        return assignee;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    @Override
    public String toString() {
        return "Task{" + "id=" + id + ", title='" + title + '\'' + ", text='" + text + '\'' + ", createdDate=" +
                createdDate + ", expirationDate=" + expirationDate + ", status=" + status + ", severity=" + severity +
                ", comments=" + comments + ", reporter=" + reporter + ", assignee=" + assignee + '}';
    }
}
