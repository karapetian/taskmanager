package com.egs.tasks.repository.entities;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String nameSurname;
    @Column
    private String email;
    @NaturalId
    @Column
    private String username;
    @Column
    private String passwordHash;

    @OneToMany( mappedBy = "reporter", cascade = CascadeType.ALL)
    private List<Task> createdTasks = new ArrayList<>();
    @OneToMany(mappedBy = "assignee", cascade = CascadeType.ALL)
    private List<Task> assignedTasks = new ArrayList<>();

    public User() {
    }

    public User(Long id, String nameSurname, String email, String username, String passwordHash, List<Task> createdTasks, List<Task> assignedTasks) {
        this.id = id;
        this.nameSurname = nameSurname;
        this.email = email;
        this.username = username;
        this.passwordHash = passwordHash;
        this.createdTasks = createdTasks;
        this.assignedTasks = assignedTasks;
    }

    public void addToCreatedTasks(Task newTask){
        this.createdTasks.add(newTask);
        newTask.setReporter(this);
    }

    public void addToAssignedTasks(Task taskToAssign){
        this.assignedTasks.add(taskToAssign);
        taskToAssign.setAssignee(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String password) {
        this.passwordHash = password;
    }

    public List<Task> getCreatedTasks() {
        return createdTasks;
    }

    public void setCreatedTasks(List<Task> createdTasks) {
        this.createdTasks = createdTasks;
    }

    public List<Task> getAssignedTasks() {
        return assignedTasks;
    }

    public void setAssignedTasks(List<Task> assignedTasks) {
        this.assignedTasks = assignedTasks;
    }
}
