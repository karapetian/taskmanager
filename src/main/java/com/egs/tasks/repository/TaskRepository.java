package com.egs.tasks.repository;

import com.egs.tasks.commons.Status;
import com.egs.tasks.repository.entities.Task;

import java.util.List;

public interface TaskRepository {

    Long createTask(Task task);

    Task getTask(Long id);

    List<Task> getAssignedTasks(Long userId);

    List<Task> getReportedTasks(Long userId);

    void updateTask(Task task);

    void deleteTask(Task task);

    void changeStatus(Task task, Status newStatus);

    int addComment(Long taskId, String comment);
}
