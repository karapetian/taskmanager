package com.egs.tasks.repository;

import com.egs.tasks.repository.entities.Task;
import com.egs.tasks.repository.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    Long createUser(User user);

    User getUser(Long id);

    void updateUser(User newUser);

    void deleteUser(User user);

    List<Object[]> getAllUsernames();

    Optional<User> checkCredentials(String username, String passwordHash);

    Optional<User> getUserByUsername(String username);

    boolean isExistingUserByGivenUsername(String username);

    void assignTask(Task task, User reporter, User assignee);


}
