package com.egs.tasks.repository.impl;

import com.egs.tasks.commons.Status;
import com.egs.tasks.repository.TaskRepository;
import com.egs.tasks.repository.entities.Task;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.CriteriaUpdateImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepositoryImpl implements TaskRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public TaskRepositoryImpl() {
        System.out.println("TaskRepositoryImpl");
    }

    @Override
    public Long createTask(Task task) {
        entityManager.persist(task);
        entityManager.flush();
        return task.getId();
    }

    @Override
    public Task getTask(Long id) {
        Task task = entityManager.find(Task.class, id);
        return task;
    }

    @Override
    public List<Task> getAssignedTasks(Long userId) {
        Query query = entityManager.createNativeQuery("SELECT * FROM task t WHERE assignee_id=?", Task.class);
        query.setParameter(1, userId);
        List<Task> tasks = query.getResultList();
        return tasks;
    }

    @Override
    public List<Task> getReportedTasks(Long userId) {
        Query query = entityManager.createNativeQuery("SELECT * FROM task t WHERE reporter_id=?", Task.class);
        query.setParameter(1, userId);
        List<Task> tasks = query.getResultList();
        return tasks;
    }

    @Override
    public void updateTask(Task task) {
        entityManager.merge(task);
    }

    @Override
    public void deleteTask(Task task) {
        entityManager.remove(task);
    }

    // if we can update without getting from db????
    @Override
    public void changeStatus(Task task, Status newStatus) {
        Query query = entityManager.createQuery("UPDATE Task set status= :newStatus where id=:id");
        query.setParameter("newStatus", newStatus);
        query.setParameter("id", task.getId());
        int result = query.executeUpdate();
    }

    @Override
    public int addComment(Long taskId, String comment) {
        Query query = entityManager.createNativeQuery("INSERT INTO task_comments(Task_id, comments) VALUES (?, ?)");
        query.setParameter(1, taskId);
        query.setParameter(2, comment);
        int updatedRows = query.executeUpdate();
        return updatedRows;
    }
}
