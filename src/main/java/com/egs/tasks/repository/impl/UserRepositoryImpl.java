package com.egs.tasks.repository.impl;

import com.egs.tasks.repository.UserRepository;
import com.egs.tasks.repository.entities.Task;
import com.egs.tasks.repository.entities.User;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @PersistenceContext(unitName = "Task_Manager_PERSISTENCE")
    private EntityManager entityManager;

    public UserRepositoryImpl() {
        System.out.println("UserRepositoryImpl");
    }

    @Override
    public Long createUser(User user) {
        entityManager.persist(user);
        entityManager.flush();
        return user.getId();
    }

    @Override
    public User getUser(Long id) {
        User user = entityManager.find(User.class, id);
        return user;
    }

    @Override
    public void updateUser(User newUser) {
        entityManager.merge(newUser);
    }

    @Override
    public void deleteUser(User user) {
        entityManager.remove(user);
    }

    @Override
    public List<Object[]> getAllUsernames() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Object[]> criteria = builder.createQuery(Object[].class);
        Root<User> userRoot = criteria.from(User.class);
        Path<Long> path = userRoot.get("username");
        criteria.select(builder.array(path));
        criteria.getOrderList();
        List<Object[]> valueArray = entityManager.createQuery(criteria).getResultList();
        return valueArray;
    }

    @Override
    public Optional<User> checkCredentials(String username, String passwordHash) {
        Query query = entityManager.createNativeQuery("SELECT * FROM user u WHERE username=? AND passwordHash=?", User.class);
        query.setParameter(1, username);
        query.setParameter(2, passwordHash);
        User user;
        try {
            user = (User) query.getSingleResult();
        } catch (NoResultException e) {
            //log
            return Optional.empty();
        }
        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> getUserByUsername(String username){
        Session session = entityManager.unwrap(Session.class);
        User user = session.bySimpleNaturalId(User.class).load(username);
        return Optional.ofNullable(user);
    }

    @Override
    public boolean isExistingUserByGivenUsername(String username) {
        Optional<User> optionalUser = getUserByUsername(username);
        if (optionalUser.isPresent()) {
            return true;
        }
        return false;
    }

    @Override
    public void assignTask(Task task, User reporter, User assignee) {
    }
}
