package com.egs.tasks.commons;

public enum Severity {
    EASY,
    MEDIUM,
    HARD,
    NONE
}
