package com.egs.tasks.commons;

public enum Status {

    NEW,        // when task does not have an assignee
    ASSIGNED,   // when newly created task has an assignee
    REASSIGNED, // when the 1st assignee reassigned task to another  user
    UPDATED     // when creator updates its task
}
