package com.egs.tasks.springConfigs;

import com.egs.tasks.utils.Mapper;
import com.egs.tasks.utils.MapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MappingConfig {

  @Bean
  public Mapper beanMapper() {
    return new MapperImpl();
  }
}
