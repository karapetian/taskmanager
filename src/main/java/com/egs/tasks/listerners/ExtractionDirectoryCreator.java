package com.egs.tasks.listerners;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.annotation.WebListener;
import java.io.File;

@WebListener
public class ExtractionDirectoryCreator implements ServletRequestAttributeListener {

    @Override
    public void attributeAdded(ServletRequestAttributeEvent srae) {
        if(srae.getName().equals("extractionPath")){
            String extractionPath = (String)srae.getValue();
            File directory = new File(extractionPath);
            if (! directory.exists()){
               directory.mkdirs();
            }
        }
    }

    @Override
    public void attributeRemoved(ServletRequestAttributeEvent srae) {

    }

    @Override
    public void attributeReplaced(ServletRequestAttributeEvent srae) {

    }
}
