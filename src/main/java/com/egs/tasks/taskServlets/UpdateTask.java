package com.egs.tasks.taskServlets;

import com.egs.tasks.commons.Severity;
import com.egs.tasks.commons.Status;
import com.egs.tasks.service.TaskService;
import com.egs.tasks.service.UserService;
import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.service.dtoes.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

/**
 * The user updates a task which was assigned to him.
 */
@WebServlet("/updateTask")
public class UpdateTask extends HttpServlet {

    private TaskService taskService;
    private UserService userService;
    private Long taskId;
    private String initialAssigneeUsername;
    private Status initialStatus;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        userService = applicationContext.getBean(UserService.class);
        taskService = applicationContext.getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        taskId = Long.valueOf(req.getParameter("id"));
        TaskDto task = taskService.getTask(taskId);
        initialAssigneeUsername = task.getAssignee().getUsername();
        initialStatus = task.getStatus();
        req.setAttribute("taskToUpdate", task);
        req.getRequestDispatcher("updateTask.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        Status status = initialStatus;
        String title = req.getParameter("title");
        String text = req.getParameter("text");
        String severity = req.getParameter("severity");
        String assigneeUsername = req.getParameter("assignee");
        if (!initialAssigneeUsername.equals(assigneeUsername)) {
            status = Status.REASSIGNED;
        }

        String expDateParam = req.getParameter("expirationDate");
        Date expirationDate = null;
        try {
            expirationDate = new SimpleDateFormat("MM/dd/yyyy").parse(expDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
        Optional<UserDto> assigneeUserOptional = userService.getUserByUsername(assigneeUsername);
        UserDto assignee = null;
        if (!assigneeUserOptional.isPresent()) {
            // log
            // ???
        } else {
            assignee = assigneeUserOptional.get();
        }
        Date date = new Date();
        TaskDto updatedTask = new TaskDto(taskId, title, text, date, expirationDate, status, Severity.valueOf(severity), Arrays.asList(), currentUser, assignee);

        taskService.updateTask(updatedTask);
        req.setAttribute("taskToUpdate", updatedTask);
        req.setAttribute("isTaskUpdated", true);
        req.getRequestDispatcher("updateTask.jsp").forward(req, resp);
    }
}

