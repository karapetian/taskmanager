package com.egs.tasks.taskServlets;

import com.egs.tasks.service.TaskService;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addComment")
public class AddCommentServlet extends HttpServlet {

    private TaskService taskService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        taskService = applicationContext.getBean(TaskService.class);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newComment = req.getParameter("newComment");
        Long taskId = Long.valueOf(req.getParameter("id"));
        taskService.addComment(taskId, newComment);

        resp.sendRedirect("updateTask?id="+ taskId);
    }
}
