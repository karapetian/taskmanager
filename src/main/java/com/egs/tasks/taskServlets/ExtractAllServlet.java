package com.egs.tasks.taskServlets;

import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.service.dtoes.UserDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@WebServlet("/extractAll")
public class ExtractAllServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        UserDto currentUser = (UserDto) (req.getSession().getAttribute("currentUser"));
        List<TaskDto> extractionTasks = Collections.emptyList();
        String hiddenParamValue = req.getParameter("taskId");
        if ("extractAssignedTasks".equals(hiddenParamValue)) {
            extractionTasks = currentUser.getAssignedTasks();
            resp.setHeader("Content-disposition", "attachment; filename=assigned_tasks.xml");
        } else if ("extractReportedTasks".equals(hiddenParamValue)) {
            extractionTasks = currentUser.getCreatedTasks();
            resp.setHeader("Content-disposition", "attachment; filename=reported_tasks.xml");
        } else {
            resp.sendRedirect("home.jsp");
            return;
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(TaskDto[].class);
            JAXBElement<TaskDto[]> root = new JAXBElement<TaskDto[]>(new QName("tasks"),
                    TaskDto[].class, extractionTasks.toArray(new TaskDto[extractionTasks.size()]));
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(root, resp.getOutputStream());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
