package com.egs.tasks.taskServlets;

import com.egs.tasks.commons.Severity;
import com.egs.tasks.commons.Status;
import com.egs.tasks.service.TaskService;
import com.egs.tasks.service.UserService;
import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.service.dtoes.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

@WebServlet("/createTask")
public class CreateTaskServlet extends HttpServlet {

    private TaskService taskService;
    private UserService userService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        taskService = applicationContext.getBean(TaskService.class);
        userService = applicationContext.getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.getSession().setAttribute("usernamesList", userService.getAllUsernames());  // in init() method ???
        req.getRequestDispatcher("createTask.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        String title = req.getParameter("title");
        String text = req.getParameter("text");
        String severity = req.getParameter("severity");
        String assigneeUsername = req.getParameter("assignee");

        String expDateParam = req.getParameter("expirationDate");
        Date expirationDate = null;
        try {
            expirationDate = new SimpleDateFormat("MM/dd/yyyy").parse(expDateParam);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
        Optional<UserDto> assigneeUserOptional = userService.getUserByUsername(assigneeUsername);
        UserDto assignee = null;
        Status status = Status.NEW;
        if (!assigneeUserOptional.isPresent()) {
            // log
            // ???
        } else {
            assignee = assigneeUserOptional.get();
            status = Status.ASSIGNED;
        }
        Date creationDate = new Date();
        TaskDto newTask = new TaskDto(title, text, creationDate, expirationDate, status, Severity.valueOf(severity), Arrays.asList(), currentUser, assignee);

        taskService.createTask(newTask);

        resp.sendRedirect("createTask.jsp");
        req.getSession().setAttribute("isTaskCreated", true);
    }
}
