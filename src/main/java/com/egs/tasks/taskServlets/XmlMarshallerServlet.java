package com.egs.tasks.taskServlets;

import com.egs.tasks.service.TaskService;
import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.service.dtoes.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/marshall")
public class XmlMarshallerServlet extends HttpServlet {

    private TaskService taskService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        taskService = applicationContext.getBean(TaskService.class);
    }


    /*
    Saves the extracted file in Downloads folder.
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TaskDto task = taskService.getTask(Long.valueOf(req.getParameter("taskId")));

        resp.setContentType("application/xml");
        resp.setHeader("Content-disposition", "attachment; filename=task_" + task.getTitle() + ".xml");

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(TaskDto.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(task, resp.getOutputStream());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /*
    Saves the extracted file in given directory.
 */
//    @Override
    protected void doGet1(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long taskId = Long.valueOf(req.getParameter("taskId"));
        TaskDto task = taskService.getTask(taskId);
        UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
        String currentUsername = currentUser.getUsername();
        String extractionPath = new StringBuilder("C:\\TaskManager\\").append(currentUsername).append("\\xml").toString();
        req.setAttribute("extractionPath", extractionPath);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(TaskDto.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            String currentTime = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss").format(new Date());
            String filepath = new StringBuilder(extractionPath).append("\\task").append(taskId).append("_")
                    .append(currentTime).append(".xml").toString();
            File file = new File(filepath);
            jaxbMarshaller.marshal(task, file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        resp.sendRedirect("home.jsp");
    }
}
