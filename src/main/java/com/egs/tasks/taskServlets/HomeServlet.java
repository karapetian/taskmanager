package com.egs.tasks.taskServlets;

import com.egs.tasks.service.TaskService;
import com.egs.tasks.service.UserService;
import com.egs.tasks.service.dtoes.UserDto;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {

    private TaskService taskService;
    private UserService userService;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletConfig.getServletContext());
        userService = applicationContext.getBean(UserService.class);
        taskService = applicationContext.getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long currentUserId = ((UserDto)req.getSession().getAttribute("currentUser")).getId();
        req.getSession().setAttribute("assignedTasks", taskService.getAssignedTasks(currentUserId));  // in init() method ???
        req.getSession().setAttribute("reportedTasks", taskService.getReportedTasks(currentUserId));
        req.getRequestDispatcher("home.jsp").forward(req, resp);

        req.getSession().setAttribute("usernamesList", userService.getAllUsernames());  //for createTask and updateTask
    }
}
