package com.egs.tasks.service.impl;

import com.egs.tasks.commons.Status;
import com.egs.tasks.repository.TaskRepository;
import com.egs.tasks.repository.entities.Task;
import com.egs.tasks.service.TaskService;
import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {
    @Autowired
    private Mapper mapper;
    @Autowired
    private TaskRepository taskRepository;

    public TaskServiceImpl() {
        System.out.println("TaskServiceImpl");
    }

    @Override
    @Transactional
    public Long createTask(TaskDto task) {
        Task taskToSave = mapper.map(task, Task.class);
        Long id = taskRepository.createTask(taskToSave);
        return id;
    }

    @Override
    @Transactional
    public TaskDto getTask(Long id) {
        Task task = taskRepository.getTask(id);
        TaskDto taskDto = mapper.map(task, TaskDto.class);
        return taskDto;
    }

    @Override
    @Transactional
    public List<TaskDto> getAssignedTasks(Long userId){
        List<Task> tasks = taskRepository.getAssignedTasks(userId);
        List<TaskDto> taskDtos = mapper.map(tasks, TaskDto.class);
        return taskDtos;
    }

    @Override
    @Transactional
    public List<TaskDto> getReportedTasks(Long userId) {
        List<Task> tasks = taskRepository.getReportedTasks(userId);
        List<TaskDto> taskDtos = mapper.map(tasks, TaskDto.class);
        return taskDtos;
    }

    @Override
    @Transactional
    public void updateTask(TaskDto task) {
        Task taskToUpdate = mapper.map(task, Task.class);
        taskRepository.updateTask(taskToUpdate);
    }

    @Override
    @Transactional
    public void deleteTask(TaskDto task) {
        Task taskToDelete = mapper.map(task, Task.class);
        taskRepository.deleteTask(taskToDelete);
    }

    @Override
    @Transactional
    public void changeStatus(TaskDto task, Status newStatus) {
        Task taskToUpdate = mapper.map(task, Task.class);
        taskRepository.changeStatus(taskToUpdate, newStatus);
    }

    @Override
    @Transactional
    public void addComment(Long taskId, String comment) {
        taskRepository.addComment(taskId, comment);
    }
}
