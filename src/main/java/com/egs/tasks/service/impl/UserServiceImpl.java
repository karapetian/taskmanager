package com.egs.tasks.service.impl;

import com.egs.tasks.repository.TaskRepository;
import com.egs.tasks.repository.UserRepository;
import com.egs.tasks.repository.entities.Task;
import com.egs.tasks.repository.entities.User;
import com.egs.tasks.service.UserService;
import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.service.dtoes.UserDto;
import com.egs.tasks.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private Mapper mapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TaskRepository taskRepository;

    public UserServiceImpl() {
        System.out.println("UserServiceImpl");
    }

    @Override
    @Transactional
    public Long createUser(UserDto user) {
        User userToSave = mapper.map(user, User.class);
        Long id = userRepository.createUser(userToSave);
        return id;
    }

    @Override
    @Transactional
    public UserDto getUser(Long id) {
        User user = userRepository.getUser(id);
        UserDto userDto = mapper.map(user, UserDto.class);
        return userDto;
    }

    @Override
    @Transactional
    public void updateUser(UserDto newUser) {
        User userToUpdate = mapper.map(newUser, User.class);
        userRepository.updateUser(userToUpdate);
    }

    @Override
    @Transactional
    public void deleteUser(UserDto user) {
        User userToDelete = mapper.map(user, User.class);
        userRepository.deleteUser(userToDelete);
    }

    @Override
    @Transactional
    public List<Object[]> getAllUsernames() {
        return userRepository.getAllUsernames();
    }

    @Override
    @Transactional
    public Optional<UserDto> checkCredentials(String username, String passwordHash) {
        Optional<User> optionalUser = userRepository.checkCredentials(username, passwordHash);
        if (!optionalUser.isPresent()) {
            return Optional.empty();
        }
        User user = optionalUser.get();
        UserDto userDto = mapper.map(user, UserDto.class);
        return Optional.ofNullable(userDto);
    }

    @Override
    @Transactional
    public Optional<UserDto> getUserByUsername(String username) {
        Optional<User> optionalUser = userRepository.getUserByUsername(username);
        if (!optionalUser.isPresent()) {
            return Optional.empty();
        }
        UserDto user = mapper.map(optionalUser.get(), UserDto.class);
        return Optional.of(user);
    }

    @Override
    @Transactional
    public boolean isExistingUserByGivenUsername(String username) {
        return userRepository.isExistingUserByGivenUsername(username);
    }

    @Override
    @Transactional
    public void assignTask(TaskDto task, UserDto reporter, UserDto assignee) {
        reporter.addToCreatedTasks(task);  //???  hanel
        assignee.addToAssignedTasks(task);
        User userReporter = mapper.map(reporter, User.class);
        User userToAssign = mapper.map(assignee, User.class);
        Task taskToAssign = mapper.map(task, Task.class);
        userRepository.updateUser(userReporter);  //??? hanel
        userRepository.updateUser(userToAssign);
        taskRepository.updateTask(taskToAssign);
    }
}
