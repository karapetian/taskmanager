package com.egs.tasks.service;

import com.egs.tasks.repository.entities.User;
import com.egs.tasks.service.dtoes.TaskDto;
import com.egs.tasks.service.dtoes.UserDto;

import java.util.List;
import java.util.Optional;

public interface UserService {
    Long createUser(UserDto user);

    UserDto getUser(Long id);

    void updateUser(UserDto newUser);

    void deleteUser(UserDto user);

    List<Object[]> getAllUsernames();

    Optional<UserDto> checkCredentials(String username, String passwordHash);

    Optional<UserDto> getUserByUsername(String username);

    boolean isExistingUserByGivenUsername(String username);

    void assignTask(TaskDto task, UserDto reporter, UserDto assignee);

//    Long createTask(TaskDto taskDto);
}
