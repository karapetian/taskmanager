package com.egs.tasks.service.dtoes;

import com.egs.tasks.commons.Severity;
import com.egs.tasks.commons.Status;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"id", "createdDate", "title", "text", "status", "severity", "expirationDate", "comments", "reporter", "assignee"})
public class TaskDto {
    @XmlAttribute
    private Long id;
    @XmlElement
    private String title;
    @XmlElement
    private String text;
    @XmlElement
    private Date createdDate;
    @XmlElement
    private Date expirationDate;
    @XmlElement
    private Status status;
    @XmlElement
    private Severity severity;
    @XmlElementWrapper(name = "taskComments")
    @XmlElement(name = "comment")
    private List<String> comments = new ArrayList<>();

        @XmlElement
//    @XmlTransient
    private UserDto reporter;
        @XmlElement
//    @XmlTransient
    private UserDto assignee;

    public TaskDto() {
    }

    public TaskDto(String title, String text, Date createdDate, Date expirationDate, Status status, Severity severity, List<String> comments, UserDto reporter, UserDto assignee) {
        this.title = title;
        this.text = text;
        this.createdDate = createdDate;
        this.expirationDate = expirationDate;
        this.status = status;
        this.severity = severity;
        this.comments = comments;
        this.reporter = reporter;
        this.assignee = assignee;
    }

    public TaskDto(Long id, String title, String text, Date createdDate, Date expirationDate, Status status, Severity severity, List<String> comments, UserDto reporter, UserDto assignee) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.createdDate = createdDate;
        this.expirationDate = expirationDate;
        this.status = status;
        this.severity = severity;
        this.comments = comments;
        this.reporter = reporter;
        this.assignee = assignee;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public UserDto getReporter() {
        return reporter;
    }

    public void setReporter(UserDto reporter) {
        this.reporter = reporter;
    }

    public UserDto getAssignee() {
        return assignee;
    }

    public void setAssignee(UserDto assignee) {
        this.assignee = assignee;
    }

    @Override
    public String toString() {
        return "TaskDto{" + "id=" + id + ", title='" + title + '\'' + ", text='" + text + '\'' + ", createdDate=" +
                createdDate + ", expirationDate=" + expirationDate + ", status=" + status + ", severity=" + severity +
                ", comments=" + comments + ", reporter=" + reporter + ", assignee=" + assignee + '}';
    }
}
