package com.egs.tasks.service.dtoes;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"id", "username", "nameSurname", "email",})
public class UserDto {
    @XmlAttribute
    private Long id;
    @XmlElement
    private String nameSurname;
    @XmlElement
    private String email;
    @XmlElement(required = true)
    private String username;
    @XmlTransient
    private String passwordHash;
    //    @XmlElementWrapper(name = "createdTasks")
//    @XmlElement(name = "task")
    @XmlTransient
    private List<TaskDto> createdTasks = new ArrayList<>();
    //    @XmlElementWrapper(name = "assignedTasks")
//    @XmlElement(name = "task")
    @XmlTransient
    private List<TaskDto> assignedTasks = new ArrayList<>();

    public UserDto() {
    }

    public UserDto(String nameSurname, String email, String username, String passwordHash) {
        this.nameSurname = nameSurname;
        this.email = email;
        this.username = username;
        this.passwordHash = passwordHash;
    }

    public void addToCreatedTasks(TaskDto newTask) {
        this.createdTasks.add(newTask);
        newTask.setReporter(this);
    }

    public void addToAssignedTasks(TaskDto taskToAssign) {
        this.assignedTasks.add(taskToAssign);
        taskToAssign.setAssignee(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameSurname() {
        return nameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.nameSurname = nameSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public List<TaskDto> getCreatedTasks() {
        return createdTasks;
    }

    public void setCreatedTasks(List<TaskDto> createdTasks) {
        this.createdTasks = createdTasks;
    }

    public List<TaskDto> getAssignedTasks() {
        return assignedTasks;
    }

    public void setAssignedTasks(List<TaskDto> assignedTasks) {
        this.assignedTasks = assignedTasks;
    }
}
