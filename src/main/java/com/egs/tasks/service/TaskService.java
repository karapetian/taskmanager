package com.egs.tasks.service;

import com.egs.tasks.commons.Status;
import com.egs.tasks.service.dtoes.TaskDto;

import java.util.List;

public interface TaskService {

    Long createTask(TaskDto task);

    TaskDto getTask(Long id);

    List<TaskDto> getAssignedTasks(Long userId);

    List<TaskDto> getReportedTasks(Long userId);

    void updateTask(TaskDto task);

    void deleteTask(TaskDto task);

    void changeStatus(TaskDto task, Status newStatus);

    void addComment(Long taskId, String comment);
}
