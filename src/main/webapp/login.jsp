<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LogIn</title>
</head>
<body>
<center>
    <form action="<%=request.getContextPath()%>/login" target="_self" method="post">
        <fieldset>
            <legend>LogIn</legend>
            <B>Username</B>
            <input type="text" name="username">
            <br>
            <B>Password</B>
            <input type="password" name="password">
            <br>
            <input type="submit" style="color:white; background-color:#4CAF50" value="Submit">
        </fieldset>
    </form>
    <%--must be Boolean not boolean--%>
    <% Boolean isSuccessful = (Boolean) session.getAttribute("isSuccessful"); %>
    <c:set var="isSuccessful" scope="session" value="<%=isSuccessful%>"/>
    <c:if test="${isSuccessful==false}">
        <p style="color:red">Wrong Login or Password ! </p>
    </c:if>
    <c:if test="${isExistingUsername==false}">
        <h3><B><p style="color:#4CAF50"> Your account is created. Now you can Log In. </p></B></h3>
    </c:if>
</center>

<h4> Are you here for the first time? </h4>
<form action="<%=request.getContextPath()%>/signup" target="_self" method="get">
    <input type="submit" style="color:white; background-color:#4CAF50; padding:3px; border-radius:2px"
           value="Create New Account">
</form>
</body>
</html>
