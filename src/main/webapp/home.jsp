<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="com.egs.tasks.service.dtoes.TaskDto" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>
<body>
<% request.getSession().setAttribute("isTaskCreated", false); %>

<fieldset>
    <form align="right" method="get" action="<%=request.getContextPath()%>/createTask">
        <input type="submit" style="color:white; background-color:#4CAF50" value="Create Task"/>
    </form>
    <form align="right" method="get" action="<%=request.getContextPath()%>/logout">
        <input type="submit" style="color:white; background-color:#4CAF50" value="Log Out"/>
    </form>
</fieldset>

<fieldset style="margin-top: 5%">
    <form method="get" action="<%=request.getContextPath()%>/extractAll">
        <h2> Here are tasks assigned to you: </h2>
        <input type="submit" value="Extract all">
        <input type="hidden" name="taskId" value="extractAssignedTasks"/>
    </form>

    <% List<TaskDto> assignedTasks = (List<TaskDto>) session.getAttribute("assignedTasks");%>

    <c:forEach var="task" items="<%=assignedTasks%>">
        <form method="get" action="<%=request.getContextPath()%>/marshall">
            <input type="hidden" name="taskId" value="${task.getId()}" />
                ${task.getId()}. <span><a href="<%=request.getContextPath()%>/updateTask?id=${task.getId()}" name="update"
                                          target="_self"> ${task.title}</a></span>
            <input type="submit" value="Extract XML">
        </form>
        <br>
    </c:forEach>
</fieldset>

<fieldset style="margin-top: 5%">
    <form method="get" action="<%=request.getContextPath()%>/extractAll">
        <h2> Here are your reported tasks: </h2>
        <input type="submit" value="Extract all">
        <input type="hidden" name="taskId" value="extractReportedTasks"/>
    </form>

    <% List<TaskDto> reportedTasks = (List<TaskDto>) session.getAttribute("reportedTasks");%>

    <c:forEach var="task" items="<%=reportedTasks%>">
        <form method="get" action="<%=request.getContextPath()%>/marshall">
            <input type="hidden" name="taskId" value="${task.getId()}" />
                ${task.getId()}. <span><a href="<%=request.getContextPath()%>/updateTask?id=${task.getId()}" name="update"
                                          target="_self"> ${task.title}</a></span>
            <input type="submit" value="Extract XML">
        </form>
        <br>
    </c:forEach>
</fieldset>
</body>
</html>
