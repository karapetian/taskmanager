<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Create New Task</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
</head>
<body>
<form action="<%=request.getContextPath()%>/createTask" target="_self" method="post">
    <fieldset>
        <legend>Create new task</legend>
        Title: <input type="text" name="title">
        <br>
        Expiration date: <input type="text" id="datepicker" name="expirationDate">
        <br>
        <label for="textarea">Task Description:</label>
        <br>
        <textarea id="textarea" name="text" rows="7" cols="70" placeholder="Describe the task here..."
                  autofocus required></textarea>
        <br>
        <br>
        <B>Set Severity </B>
        <select name="severity" required>
            <option value="NONE" selected>Select</option>
            <option value="EASY">EASY</option>
            <option value="MEDIUM">MEDIUM</option>
            <option value="HARD">HARD</option>
        </select>

        <br>
        <br>
        <B>Assign task to... </B>
        <% List<String> usernames = (List<String>) session.getAttribute("usernamesList");%>
        <select name='assignee'>
            <option value="NONE" selected>Select</option>
            <c:forEach var="username" items="<%=usernames%>">
                <option value="${username}">${username}</option>
            </c:forEach>
        </select>

        <br>
        <input type="submit" style="color:white; background-color:#4CAF50" value="Create">
    </fieldset>
</form>
<% Boolean isTaskCreated = (Boolean) session.getAttribute("isTaskCreated"); %>
<c:set var="isTaskCreated" value="<%=isTaskCreated%>"/>
<c:if test="${isTaskCreated==true}">
    <p style="color:#4CAF50">The task has been created successfully !</p>
</c:if>
<form align="left" action="<%=request.getContextPath()%>/home" target="_self" method="get">
    <input type="submit" style="color:white; background-color:#4CAF50" value="Back">
</form>
</body>
</html>
