<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Sign Up</title>
</head>
<body>
<center>
    <form action="<%=request.getContextPath()%>/signup" target="_self" method="post">
        <fieldset>
            <legend>Create your account</legend>
            <B>Name</B>
            <input type="text" name="nameSurname" required>
            <br>
            <B>E-mail</B>
            <input type="email" name="email" required>
            <br>
            <B>Username</B>
            <input type="text" name="username" required>
            <br>
            <B>Password</B>
            <input type="password" name="password" required>
            <br>
            <input type="submit" style="color:white; background-color:#4CAF50" value="SignUp">
        </fieldset>
    </form>
    <% Boolean isExistingUsername = (Boolean) session.getAttribute("isExistingUsername"); %>
    <% String username = request.getParameter("username"); %>
    <c:set var="isExistingUsername" scope="session" value="<%=isExistingUsername%>"/>
    <c:if test="${isExistingUsername==true}">
        <p style="color:red">User with <b><%=username%></b>  username already exists. Try another one. </p>
    </c:if>
</center>
</body>
</html>
