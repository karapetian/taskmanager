<%@ page import="java.util.List" %>
<%@ page import="com.egs.tasks.service.dtoes.TaskDto" %>
<%@ page import="com.egs.tasks.commons.Severity" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
    <title>Update Task</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="static/css/updateTask.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
</head>
<body>
<%TaskDto taskToUpdate = (TaskDto) request.getAttribute("taskToUpdate"); %>
<%String text = taskToUpdate.getText(); %>
<form action="<%=request.getContextPath()%>/updateTask" target="_self" method="post">
        <legend>Update task</legend>
        <label>Task Id: <b><%=taskToUpdate.getId()%>
        </b></label>
        <br>
        Title: <input type="text" name="title" value="<%=taskToUpdate.getTitle()%>">
        <br>
        Expiration date: <input type="text" id="datepicker" name="expirationDate" value=
         "<%=new SimpleDateFormat("MM/dd/yyyy").format(taskToUpdate.getExpirationDate())%>">
        <br>
        <label for="textarea">Task Description:</label>
        <br>
        <textarea id="textarea" name="text" rows="7" cols="70" placeholder="Describe the task here..."
                  autofocus required><%=text%></textarea>
        <br>
        <br>
        <% Severity[] severities = Severity.values();%>
        <B>Set Severity </B>
        <select name="severity" required>
            <c:forEach var="svt" items="<%=severities%>">
                <c:choose>
                    <c:when test="${svt.equals(taskToUpdate.severity)}">
                        <option value="${svt.name()}" selected>${svt.name()}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${svt.name()}">${svt.name()}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>

        <br>
        <br>
        <B>Assign task to... </B>
            <% List<String> usernames = (List<String>)session.getAttribute("usernamesList");%>
        <select name='assignee'>
            <c:forEach var="username" items="<%=usernames%>">
                <c:choose>
                    <c:when test="${username.equals(taskToUpdate.assignee.username)}">
                        <option value="${username}" selected>${username}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${username}">${username}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
        <br>
        <br>
        <input type="submit" style="color:white; background-color:#4CAF50" value="Update">
</form>
<br>
<br>
<form method="post" action="<%=request.getContextPath()%>/addComment?id=${taskToUpdate.getId()}">
    <fieldset class="comments">
        <legend>Comments ...</legend>
        <% List<String> comments = taskToUpdate.getComments();%>
        <% request.setAttribute("taskToUpdateId", taskToUpdate.getId());%>
        <c:forEach var="comment" items="<%=comments%>">
            <p class="border"><span> ${comment} </span></p>
        </c:forEach>
        <br>
        <B>Write a new comment...</B>
        <input type="text" name="newComment">
        <br>
        <input type="submit" style="color:white; background-color:#4CAF50;" value="Add">
    </fieldset>
</form>
<br> <br>
<br> <br>
<form action="<%=request.getContextPath()%>/home" method="get">
    <input type="submit" style="color:white; background-color:#4CAF50" value="Cancel">
</form>

<% Boolean isTaskUpdated = (Boolean) request.getAttribute("isTaskUpdated"); %>
<c:set var="isTaskUpdated" value="<%=isTaskUpdated%>"/>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p style="color:gray">The task has been updated successfully !</p>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
        </div>
    </div>
</div>
<c:if test="${isTaskUpdated}">
    <script>$('#exampleModal').modal('show')</script>
</c:if>
</body>
</html>
